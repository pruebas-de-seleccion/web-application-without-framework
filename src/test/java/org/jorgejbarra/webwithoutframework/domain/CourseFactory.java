package org.jorgejbarra.webwithoutframework.domain;

import com.github.javafaker.Faker;
import java.util.Locale;

public final class CourseFactory {
  private CourseFactory() {
  }

  public static Course sampleCourse() {
    final Faker usFaker = new Faker(new Locale("es-ES"));
    return new Course(
        usFaker.number().numberBetween(1L, 200L),
        usFaker.book().title(),
        usFaker.options().nextElement(Level.values()),
        true,
        usFaker.number().numberBetween(1, 48)
    );
  }

  public static Course sampleInactiveCourse() {
    final Faker usFaker = new Faker(new Locale("es-ES"));
    return new Course(
        usFaker.number().numberBetween(1L, 200L),
        usFaker.book().title(),
        usFaker.options().nextElement(Level.values()),
        false,
        usFaker.number().numberBetween(1, 48)
    );
  }
}
