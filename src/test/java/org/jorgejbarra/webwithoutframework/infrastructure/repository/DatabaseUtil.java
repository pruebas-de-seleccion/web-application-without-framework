package org.jorgejbarra.webwithoutframework.infrastructure.repository;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseUtil {
  static void clean() {
    try (final Connection connection = new DatabaseConnectionFactory().getConnection()) {
      //noinspection SqlWithoutWhere
      connection.createStatement().executeUpdate("DELETE FROM course");
    } catch (SQLException e) {
      throw new IllegalStateException("Can not clan database in test", e);
    }
  }
}
