package org.jorgejbarra.webwithoutframework.infrastructure.repository;

import java.io.File;
import java.util.Collection;
import org.jorgejbarra.webwithoutframework.domain.Course;
import org.jorgejbarra.webwithoutframework.domain.CourseRepository;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.DockerComposeContainer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jorgejbarra.webwithoutframework.domain.CourseFactory.sampleCourse;
import static org.jorgejbarra.webwithoutframework.domain.CourseFactory.sampleInactiveCourse;
import static org.jorgejbarra.webwithoutframework.infrastructure.BeanPool.getCourseRepository;
import static org.testcontainers.containers.wait.strategy.Wait.forHealthcheck;

public class CourseRepositoryShould {
  @ClassRule
  @SuppressWarnings("rawtypes")
  public static DockerComposeContainer postgres = new DockerComposeContainer(new File("docker-compose.yml"))
      .withLocalCompose(true)
      .waitingFor("postgresql", forHealthcheck());

  private final CourseRepository repository = getCourseRepository();

  @After
  public void tearDown() {
    DatabaseUtil.clean();
  }

  @Test
  public void saveCourse() {
    //Given
    final Course courseToSave = sampleCourse();

    //When
    repository.save(courseToSave);

    //Then
    assertThat(repository.findAll())
        .usingRecursiveFieldByFieldElementComparator()
        .containsExactly(courseToSave);
  }

  @Test
  public void retriveOnlyActiveCoruses() {
    //Given
    final Course courseToSave = sampleInactiveCourse();

    //When
    repository.save(courseToSave);

    //Then
    assertThat(repository.findAll()).isEmpty();
  }

  @Test
  public void returnEmptyListWhenRepositoryIsEmpty() {
    //When
    final Collection<Course> courses = repository.findAll();

    //Then
    assertThat(courses).isEmpty();
  }
}
