package org.jorgejbarra.webwithoutframework.infrastructure.command;

import org.jetbrains.annotations.NotNull;

public class GetUnknownPageShould extends BasePageCommandTest {

  @Override
  @NotNull Command getCommandToTest() {
    return new GetUnknownPage();
  }

  @Override
  @NotNull String getExpectedTitle() {
    return "UPS! Pagina no encontrada";
  }

  @Override
  @NotNull String getExpectedContentViewName() {
    return "unknown";
  }
}
