package org.jorgejbarra.webwithoutframework.infrastructure.command;

import java.util.AbstractMap.SimpleEntry;
import org.jetbrains.annotations.NotNull;
import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class BasePageCommandTest {

  @Test
  public void haveTemplateViewWithExpectedContentView() {
    //Given
    final Command command = getCommandToTest();

    //When
    final PageResponse response = command.execute();

    //Then
    assertThat(response.getView()).isEqualTo("template");
    assertThat(response.getModel()).contains(new SimpleEntry<>("contentView", getExpectedContentViewName()));
  }

  @Test
  public void haveCorrectTittle() {
    //Given
    final Command command = getCommandToTest();

    //When
    final PageResponse response = command.execute();

    //Then
    assertThat(response.getModel()).contains(new SimpleEntry<>("title", getExpectedTitle()));
  }

  @NotNull
  abstract Command getCommandToTest();

  @NotNull
  abstract String getExpectedTitle();

  @NotNull
  abstract String getExpectedContentViewName();
}
