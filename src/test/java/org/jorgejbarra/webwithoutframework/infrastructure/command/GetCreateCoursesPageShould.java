package org.jorgejbarra.webwithoutframework.infrastructure.command;

import java.util.AbstractMap.SimpleEntry;
import org.jetbrains.annotations.NotNull;
import org.jorgejbarra.webwithoutframework.domain.Level;
import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GetCreateCoursesPageShould extends BasePageCommandTest {

  @Test
  public void containsListOfLevelInModel() {
    //Given
    final Command command = getCommandToTest();

    //When
    final PageResponse response = command.execute();

    //Then
    assertThat(response.getModel())
        .usingRecursiveFieldByFieldElementComparator()
        .contains(new SimpleEntry<>("levelOptions", Level.values()));
  }

  @Override
  @NotNull
  Command getCommandToTest() {
    return new GetCreateCoursesPage();
  }

  @Override
  @NotNull
  String getExpectedTitle() {
    return "Crear curso";
  }

  @Override
  @NotNull
  String getExpectedContentViewName() {
    return "createCourse";
  }
}
