package org.jorgejbarra.webwithoutframework.infrastructure.command;

import javax.servlet.http.HttpServletRequest;
import org.jorgejbarra.webwithoutframework.infrastructure.MockHttpServletRequest;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jorgejbarra.webwithoutframework.infrastructure.BeanPool.getCommandFactory;

public class CommandFactoryShould {

  private final CommandFactory commandFactory = getCommandFactory();

  @Test
  public void createUnknownCommand() {
    //When
    final Command command = commandFactory.create(requestForUri("/web/unknown-path"));

    //Then
    assertThat(command).isInstanceOf(GetUnknownPage.class);
  }

  @Test
  public void createGetCreateCoursePageCommand() {
    //When
    final Command command = commandFactory.create(requestForUri("/web/createCourse"));

    //Then
    assertThat(command).isInstanceOf(GetCreateCoursesPage.class);
  }

  @Test
  public void createGetCoursesPageCommand() {
    //When
    final Command command = commandFactory.create(requestForUri("/web/listCourses"));

    //Then
    assertThat(command).isInstanceOf(GetCoursesPage.class);
  }

  private HttpServletRequest requestForUri(final String uri) {
    return new MockHttpServletRequest(uri);
  }
}
