package org.jorgejbarra.webwithoutframework.infrastructure.command;

import java.util.AbstractMap;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jorgejbarra.webwithoutframework.application.CoursesRetriever;
import org.jorgejbarra.webwithoutframework.domain.Course;
import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.jorgejbarra.webwithoutframework.domain.CourseFactory.sampleCourse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetCoursesPageShould extends BasePageCommandTest {

  private final CoursesRetriever coursesRetriever = mock(CoursesRetriever.class);

  @Test
  public void containsValueInModel() {
    //Given
    final List<Course> expectedCourses = asList(sampleCourse(), sampleCourse());
    when(coursesRetriever.execute()).thenReturn(expectedCourses);

    //When
    final PageResponse response = getCommandToTest().execute();

    //Then
    assertThat(response.getModel())
        .usingRecursiveFieldByFieldElementComparator()
        .contains(new AbstractMap.SimpleEntry<>("courses", expectedCourses));
  }


  @Override
  @NotNull
  Command getCommandToTest() {
    return new GetCoursesPage(coursesRetriever);
  }

  @Override
  @NotNull
  String getExpectedTitle() {
    return "Catálogo de cursos";
  }

  @Override
  @NotNull
  String getExpectedContentViewName() {
    return "listCourses";
  }
}
