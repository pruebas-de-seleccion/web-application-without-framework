<%--@elvariable id="courses" type="java.util.Collection<org.jorgejbarra.webwithoutframework.domain.Course>"--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h2>Catalogo de cursos</h2>

<a href="createCourse">Crear curso</a>

<table>
    <tr>
        <th>Titulo</th>
        <th>Nivel</th>
        <th>Horas</th>
    </tr>
    <c:forEach items="${courses}" var="course">
        <tr>
            <td>${course.title}</td>
            <td>${course.level}</td>
            <td>${course.durationInHours}</td>
        </tr>
    </c:forEach>
</table>
