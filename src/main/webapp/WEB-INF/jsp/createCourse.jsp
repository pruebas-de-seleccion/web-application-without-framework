<%--@elvariable id="levelOptions" type="java.util.Collection<org.jorgejbarra.webwithoutframework.domain.Level>"--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h2>Crear curso</h2>
<form method="post">
    <div class="form-group">
        <label for="titleInput">Titulo</label>
        <input type="text" id="titleInput" name="title" required>
    </div>
    <div class="form-group">
        <label for="levelInput">Nivel</label>
        <select id="levelInput" name="level" required>
            <c:forEach items="${levelOptions}" var="option">
                <option value="${option}" selected="selected">${option}</option>
            </c:forEach>
        </select>
    </div>
    <div class="form-group">
        <input type="checkbox" id="activeInput" name="active" value="baby">
        <label for="activeInput">Activo</label>
    </div>
    <div class="form-group">
        <label for="durationInHoursInput">Duración en horas</label>
        <input type="number" id="durationInHoursInput" name="durationInHours" required>
    </div>

    <div class="form-group">
        <a class="btn btn-secondary" href="listCourses">Cancelar</a>
        <button class="btn-primary" type="submit">Guardar</button>
    </div>
</form>


