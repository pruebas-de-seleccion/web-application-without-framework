<%--@elvariable id="title" type="string"--%>
<%--@elvariable id="contentView" type="string"--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>${title}</title>
</head>
<body>
<div class="wrapper">
    <jsp:include page="${contentView}.jsp"/>
</div>
</body>
</html>


