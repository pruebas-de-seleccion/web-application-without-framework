package org.jorgejbarra.webwithoutframework.application;

import java.util.Collection;
import org.jorgejbarra.webwithoutframework.domain.Course;
import org.jorgejbarra.webwithoutframework.domain.CourseRepository;

public class CoursesRetriever {
  private final CourseRepository repository;

  public CoursesRetriever(final CourseRepository repository) {
    this.repository = repository;
  }

  public Collection<Course> execute() {
    return repository.findAll();
  }
}
