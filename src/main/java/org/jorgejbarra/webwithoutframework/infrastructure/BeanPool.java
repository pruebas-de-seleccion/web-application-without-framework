package org.jorgejbarra.webwithoutframework.infrastructure;

import javax.servlet.ServletContext;
import org.jorgejbarra.webwithoutframework.application.CoursesRetriever;
import org.jorgejbarra.webwithoutframework.domain.CourseRepository;
import org.jorgejbarra.webwithoutframework.infrastructure.command.CommandFactory;
import org.jorgejbarra.webwithoutframework.infrastructure.repository.DatabaseConnectionFactory;
import org.jorgejbarra.webwithoutframework.infrastructure.repository.JdbcCourseRepository;

public class BeanPool {

  private static final JdbcCourseRepository jdbcCourseRepository = new JdbcCourseRepository(new DatabaseConnectionFactory());
  private static final CommandFactory commandFactory = new CommandFactory(new CoursesRetriever(jdbcCourseRepository));

  public static CommandFactory getCommandFactory() {
    return commandFactory;
  }

  public static CourseRepository getCourseRepository() {
    return jdbcCourseRepository;
  }

  public static RequestDispatcher getRequestDispatcher(final ServletContext servletContext) {
    return new RequestDispatcher(servletContext, commandFactory);
  }
}
