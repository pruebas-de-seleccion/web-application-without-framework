package org.jorgejbarra.webwithoutframework.infrastructure.command;

import java.util.Map;
import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;

public class GetUnknownPage implements Command {
  @Override
  public PageResponse execute() {
    return new PageResponse(
        "template",
        Map.of(
            "title", "UPS! Pagina no encontrada",
            "contentView", "unknown"
        )
    );
  }
}
