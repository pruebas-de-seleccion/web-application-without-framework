package org.jorgejbarra.webwithoutframework.infrastructure.command;

import java.util.Map;
import org.jorgejbarra.webwithoutframework.application.CoursesRetriever;
import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;

public class GetCoursesPage implements Command {
  private final CoursesRetriever coursesRetriever;

  public GetCoursesPage(final CoursesRetriever coursesRetriever) {
    this.coursesRetriever = coursesRetriever;
  }

  @Override
  public PageResponse execute() {
    return new PageResponse(
        "template",
        Map.of(
            "title", "Catálogo de cursos",
            "contentView", "listCourses",
            "courses", coursesRetriever.execute()
        )
    );
  }
}
