package org.jorgejbarra.webwithoutframework.infrastructure.command;

import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;

public interface Command {
  PageResponse execute();
}
