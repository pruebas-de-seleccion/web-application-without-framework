package org.jorgejbarra.webwithoutframework.infrastructure.command;

import java.util.Map;
import org.jorgejbarra.webwithoutframework.domain.Level;
import org.jorgejbarra.webwithoutframework.infrastructure.PageResponse;

public class GetCreateCoursesPage implements Command {

  @Override
  public PageResponse execute() {
    return new PageResponse(
        "template",
        Map.of(
            "title", "Crear curso",
            "contentView", "createCourse",
            "levelOptions", Level.values()
        )
    );
  }
}
