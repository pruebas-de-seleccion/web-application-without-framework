package org.jorgejbarra.webwithoutframework.infrastructure.command;

import javax.servlet.http.HttpServletRequest;
import org.jorgejbarra.webwithoutframework.application.CoursesRetriever;

public class CommandFactory {
  private final CoursesRetriever coursesRetriever;

  public CommandFactory(final CoursesRetriever coursesRetriever) {
    this.coursesRetriever = coursesRetriever;
  }

  public Command create(final HttpServletRequest request) {
    if (request.getRequestURI().endsWith("/createCourse")) {
      return new GetCreateCoursesPage();
    }

    if (request.getRequestURI().endsWith("/listCourses")) {
      return new GetCoursesPage(coursesRetriever);
    }

    return new GetUnknownPage();
  }
}
