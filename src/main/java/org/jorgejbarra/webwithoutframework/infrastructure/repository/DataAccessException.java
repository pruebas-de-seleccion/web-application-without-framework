package org.jorgejbarra.webwithoutframework.infrastructure.repository;

public class DataAccessException extends RuntimeException {
  DataAccessException(final Throwable cause) {
    super("Unexpected exception in data layer", cause);
  }
}
