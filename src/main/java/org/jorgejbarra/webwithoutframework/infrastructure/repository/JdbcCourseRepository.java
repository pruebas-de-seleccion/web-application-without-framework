package org.jorgejbarra.webwithoutframework.infrastructure.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.LinkedList;
import org.jorgejbarra.webwithoutframework.domain.Course;
import org.jorgejbarra.webwithoutframework.domain.CourseRepository;
import org.jorgejbarra.webwithoutframework.domain.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JdbcCourseRepository implements CourseRepository {
  private static final Logger logger = LoggerFactory.getLogger(JdbcCourseRepository.class);

  private static final String INSERT_COURSE_SQL =
      "INSERT INTO course (id, title, level, active, duration_in_hours) VALUES (?, ?, ?, ?, ?)";
  private static final String FIND_ALL_QUERY =
      "SELECT id, title, level, active, duration_in_hours FROM course";

  private final DatabaseConnectionFactory connectionFactory;

  public JdbcCourseRepository(final DatabaseConnectionFactory connectionFactory) {
    this.connectionFactory = connectionFactory;
  }

  @Override
  public void save(final Course courseToSave) {
    logger.trace("Saving " + courseToSave);
    try (
        Connection conn = connectionFactory.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(INSERT_COURSE_SQL)
    ) {

      preparedStatement.setLong(1, courseToSave.getId());
      preparedStatement.setString(2, courseToSave.getTitle());
      preparedStatement.setString(3, courseToSave.getLevel().toString().toUpperCase());
      preparedStatement.setBoolean(4, courseToSave.getActive());
      preparedStatement.setInt(5, courseToSave.getDurationInHours());

      preparedStatement.executeUpdate();

    } catch (Exception e) {
      throw new DataAccessException(e);
    }
  }

  @Override
  public Collection<Course> findAll() {
    try (
        Connection conn = connectionFactory.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(FIND_ALL_QUERY)
    ) {
      final Collection<Course> foundCourses = new LinkedList<>();

      final ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        foundCourses.add(new Course(
            resultSet.getLong("id"),
            resultSet.getString("title"),
            Level.valueOf(resultSet.getString("level")),
            resultSet.getBoolean("active"),
            resultSet.getInt("duration_in_hours")
        ));
      }
      return foundCourses;
    } catch (Exception e) {
      throw new DataAccessException(e);
    }
  }
}
