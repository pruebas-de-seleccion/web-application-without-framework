package org.jorgejbarra.webwithoutframework.infrastructure.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class DatabaseConnectionFactory {
  private static final String URL = "jdbc:postgresql://localhost:5432/web-without-frameworks";
  private static final String USER = "jorge";
  private static final String PASSWORD = "123123";

  public DatabaseConnectionFactory() {
  }

  public synchronized Connection getConnection() {
    try {
      return DriverManager.getConnection(URL, USER, PASSWORD);
    } catch (SQLException e) {
      throw new IllegalStateException("Can not connect to database", e);
    }
  }
}
