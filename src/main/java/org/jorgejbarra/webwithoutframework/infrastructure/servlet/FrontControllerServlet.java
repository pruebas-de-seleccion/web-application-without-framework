package org.jorgejbarra.webwithoutframework.infrastructure.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.jorgejbarra.webwithoutframework.infrastructure.BeanPool.getRequestDispatcher;

@WebServlet(name = "FrontControllerServlet", urlPatterns = {"/web/*"}, loadOnStartup = 1)
public class FrontControllerServlet extends HttpServlet {

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    getRequestDispatcher(getServletContext()).dispatch(request, response);
  }
}

