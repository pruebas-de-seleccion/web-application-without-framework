package org.jorgejbarra.webwithoutframework.infrastructure;

import java.io.IOException;
import java.util.Map.Entry;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jorgejbarra.webwithoutframework.infrastructure.command.Command;
import org.jorgejbarra.webwithoutframework.infrastructure.command.CommandFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestDispatcher {
  private static final Logger logger = LoggerFactory.getLogger(RequestDispatcher.class);

  private final ServletContext servletContext;
  private final CommandFactory commandFactory;

  public RequestDispatcher(final ServletContext servletContext, final CommandFactory commandFactory) {
    this.servletContext = servletContext;
    this.commandFactory = commandFactory;
  }

  public void dispatch(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    final Command command = commandFactory.create(request);
    final PageResponse pageResponse = command.execute();

    for (final Entry<String, Object> modelEntry : pageResponse.getModel()) {
      request.setAttribute(modelEntry.getKey(), modelEntry.getValue());
    }

    logger.trace("forward to :: " + pageResponse.getView());
    servletContext.getRequestDispatcher(jspFileFrom(pageResponse.getView())).forward(request, response);
  }

  private String jspFileFrom(final String view) {
    return String.format("/WEB-INF/jsp/%s.jsp", view);
  }
}
