package org.jorgejbarra.webwithoutframework.infrastructure;

import java.util.Map;
import java.util.Set;

public class PageResponse {
  private final String view;
  private final Map<String, Object> model;

  public PageResponse(final String view, final Map<String, Object> model) {
    this.view = view;
    this.model = model;
  }

  public String getView() {
    return view;
  }

  public Set<Map.Entry<String, Object>> getModel() {
    return model.entrySet();
  }
}
