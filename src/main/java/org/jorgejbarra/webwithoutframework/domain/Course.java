package org.jorgejbarra.webwithoutframework.domain;

public class Course {

  private final Long id;
  private final String title;
  private final Level level;
  private final Boolean active;
  private final Integer durationInHours;

  public Course(final Long id, final String title, final Level level, final Boolean active, final Integer durationInHours) {
    this.id = id;
    this.title = title;
    this.level = level;
    this.active = active;
    this.durationInHours = durationInHours;
  }

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public Level getLevel() {
    return level;
  }

  public Boolean getActive() {
    return active;
  }

  public Integer getDurationInHours() {
    return durationInHours;
  }

  @Override
  public String toString() {
    return "Course{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", level=" + level +
        ", active=" + active +
        ", durationInHours=" + durationInHours +
        '}';
  }
}
