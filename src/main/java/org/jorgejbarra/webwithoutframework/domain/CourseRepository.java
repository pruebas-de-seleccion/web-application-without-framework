package org.jorgejbarra.webwithoutframework.domain;

import java.util.Collection;

public interface CourseRepository {
  void save(final Course courseToSave);

  Collection<Course> findAll();
}
