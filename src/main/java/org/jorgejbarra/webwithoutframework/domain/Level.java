package org.jorgejbarra.webwithoutframework.domain;

public enum Level {
  BASIC,
  INTERMEDIATE,
  ADVANCED,
  MASTER_OF_THE_UNIVERSE
}

