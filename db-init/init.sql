DROP TABLE IF EXISTS course;
CREATE TABLE course
(
    id                char(5)     NOT NULL PRIMARY KEY,
    title             VARCHAR(40) NOT NULL,
    level             VARCHAR(25) NOT NULL,
    active            BOOLEAN     NOT NULL,
    duration_in_hours INTEGER     NOT NULL
);

