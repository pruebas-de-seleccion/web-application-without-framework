## WEB APPLICATION WITHOUT FRAMEWORK
>A little info about your project and/ or overview that explains **what** the project is about.

TBD

## Motivation
>A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

TBD

## Tech/framework used
- [gretty](https://gretty-gradle-plugin.github.io/gretty-doc/) :: Gretty is a feature-rich gradle plugin for running web-apps on embedded servlet containers. 

## Installation
TBD

## API Reference
TBD

##TODO list:
- Use any connection pool as [Apache Commons DBCP](https://commons.apache.org/proper/commons-dbcp/download_dbcp.cgi)
- Use [jOOQ](https://www.jooq.org/) to manage queries

## License
A short snippet describing the license (MIT, Apache etc)

MIT © [JorgeJBarra]()

## NOTES:
#### We may not use gretty:
We can start the embedded server without gradle creating a [main method with some code](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat)
